#ifndef _ANALOG_INPUT_
#define _ANALOG_INPUT_ 1
void adc_dmx_select_none(void);
void adc_dmx_select(uint8_t channel);

void 	adc_dmx_set_next_channel(void);
uint8_t adc_dmx_channel(void);
void adc_dmx_set_next_sample(void);

void adc_start_convertion(void);
void adc_stop_convertion(void);
void adc_update_value(void);

int32_t adc_value(uint8_t channel); // 0 .. 7
uint8_t adc_flags(void); // ovf flags

#endif