#include <malloc.h>
#include "stm32f4xx.h"
// #include "FreeRTOS.h"
#include <stdio.h>
#include "gui_slcd.h"
#include "stm32f4xx_it.h"
#include "menu_tab.h"

typedef struct
{
	uint32_t allocated;
	uint32_t freed;
	uint32_t free_heap;
}my_alloc_t;

my_alloc_t my_alloc;

void* my_malloc(size_t size)
{
	if(my_alloc.allocated - my_alloc.freed + size > 0x20000)
	{
		return NULL;
	}

	void *result = malloc(size);
	if(result)
		my_alloc.allocated += size;
	else
		while(1); // hook

	return result;
}

void my_free(void *ptr, size_t size)
{
	my_alloc.freed += size;
	free(ptr);
}

uint32_t mem_in_use(void)
{
	return my_alloc.allocated;
}

void my_alloc_init(void)
{
	my_alloc.allocated = 0;
}
