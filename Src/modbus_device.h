#ifndef __MODBUS_DEVICE__
#define __MODBUS_DEVICE__ 1

#include <stdbool.h>
#include "ext_mem.h"
#include "stm32f0xx_hal_tim.h"

#define MBD_INIT_ADDR 1

#define MBD_NUM_INPUTS 8

#define EXT_INIT_BAUDRATE 38400
#define INT_INIT_BAUDRATE 4800
#define DEFAULT_CALIB_COEFF 3148
#define DEFAULT_CALIB_OFFSET -108

#define UPDATE_BAUDRATE_FLAG (1 << 0)

typedef struct
{
    uint16_t result_id; // порядковый номер измерения
    uint16_t flags;     // флаги валидности значений
    uint32_t val;       // результат измерения 1
}input_t;

typedef struct
{
    uint8_t  addr;
    uint32_t baudrate;

    uint32_t counter;
    uint32_t start_time;
    uint16_t count_ovr; // счетчик переполнений системного тика
// ----------лежит во внешней памяти---------------------------
    uint64_t operation_time;
    uint16_t program_crc;
    uint16_t ext_mem_crc;
    uint32_t change_time; // ext mem change time
    uint32_t total_rq;   // счетчик запросов
    uint32_t handled_rq; // счетчик обработанных запросов
    uint32_t err_crc_rq; // счетчик запросов с ошибкой CRC
    uint32_t err_param_rq; // счетчик запросов с некорректными параметрами
    uint32_t err_send;  // счетчик ошибок при отправке
    uint16_t calib_coeff; // зна4ение умноженое на 1000
    int16_t  calib_offset;
// ------------------------------------------------------------

    input_t input[MBD_NUM_INPUTS];

    bool     is_init;
    uint16_t update; // for store to ext memory : used 11 bits => 8 inputs + 1 common flag + 2 tim flags
}device_t;

enum{
    RS485_ADDRESS_REG = 22,
    RS485_BAUDRATE_HREG,
    RS485_BAUDRATE_LREG,

// адреса измеренных усредненных16х значений
// при обращении к регистру
    RESULT_ID_0, // 25
    FLAGS_0,
    VAL_0_0,  // 27
    VAL_0_1,  // 28
    RESULT_ID_1,
    FLAGS_1,
    VAL_1_0,  // 31
    VAL_1_1,  // 32
    RESULT_ID_2,
    FLAGS_2,
    VAL_2_0,  // 35
    VAL_2_1,  // 36
    RESULT_ID_3,
    FLAGS_3,
    VAL_3_0,  // 39
    VAL_3_1,  // 40
    RESULT_ID_4,
    FLAGS_4,
    VAL_4_0,  // 43
    VAL_4_1,  // 44
    RESULT_ID_5,
    FLAGS_5,
    VAL_5_0,  // 47
    VAL_5_1,  // 48
    RESULT_ID_6,
    FLAGS_6,
    VAL_6_0,  // 51
    VAL_6_1,  // 52
    RESULT_ID_7,
    FLAGS_7,
    VAL_7_0,  // 55
    VAL_7_1,  // 56

    SPARE_INIT,  // 57
    CALIB_COEFF, // 58
    CALIB_OFFSET, // 59
}AI_REGISTERS;

int8_t  mbd_holding_reg(uint16_t addr, uint16_t* value);
int8_t  mbd_input_reg(uint16_t addr, uint16_t* value);
int8_t  mbd_write_reg(uint16_t addr, uint16_t value);

void mbd_inc_crc_err_rq(void);
void mbd_inc_handled_rq(void);
void mbd_inc_total_rq(void);

uint32_t update_time(void);
uint32_t operation_time(void);

void mbd_process(uint8_t channel);
void mdb_inc_send_err(void);
void mbd_common_params(common_params_t* cp);
void mbd_set_common_params(common_params_t* cp);
void mbd_log_status(void);
uint16_t mbd_module_id(void);
void mbd_log_config(void);
void mbd_set_baudrate(uint32_t baudrate);
uint8_t mbd_addr(void);

void mbd_init(void);
int8_t mbd_rs485_init(uint32_t baudrate);
void mbd_int_init(void);
void mbd_ext_init(void);
void mbd_exit_init_mode(void);
bool mbd_can_init(void);
bool mbd_is_init(void);

#endif //__MODBUS_DEVICE__
