#include "stm32f0xx.h"
#include "stm32f0xx_hal_gpio.h"
#include "adc.h"
#include "soft_timer.h"
#include "errors.h"

#define DMX_PAUSE 	1000

#define AX_B_PORT 	GPIOA
#define AX_T_PORT 	GPIOC

#define A0_B_PIN 	GPIO_PIN_11
#define A1_B_PIN 	GPIO_PIN_10
#define A2_B_PIN 	GPIO_PIN_9
#define A3_B_PIN 	GPIO_PIN_8

#define A0_T_PIN 	GPIO_PIN_9
#define A1_T_PIN 	GPIO_PIN_8
#define A2_T_PIN 	GPIO_PIN_7
#define A3_T_PIN 	GPIO_PIN_6

#define CS1_PORT	GPIOA
#define CS1_PIN		GPIO_PIN_4

#define CS2_PORT	GPIOA
#define CS2_PIN		GPIO_PIN_7

#define ADC_CHANNELS_NUM 8

#define OVH_FLAG (1 << 22)
#define OVL_FLAG (1 << 23)

#define SAMPLES_IN_BUFFER 16
#define SAMPLE_SIZE_BYTES 3

typedef struct
{
	uint8_t 	channel; // 0..7
	uint8_t 	sample; // 0..7
	uint8_t 	value[8][SAMPLES_IN_BUFFER * SAMPLE_SIZE_BYTES];
	uint8_t		result_id; // only for first measures, not for modbus
	uint8_t 	ovf_flags;
}adc_t;

adc_t adc = {0};

extern SPI_HandleTypeDef hspi1;

void adc_dmx_select(uint8_t channel)
{
	HAL_GPIO_WritePin(AX_B_PORT, A0_B_PIN, (channel & (1 << 0)));
	HAL_GPIO_WritePin(AX_B_PORT, A1_B_PIN, (channel & (1 << 1)));
	HAL_GPIO_WritePin(AX_B_PORT, A2_B_PIN, (channel & (1 << 2)));
	HAL_GPIO_WritePin(AX_B_PORT, A3_B_PIN, (channel & (1 << 3)));

	HAL_GPIO_WritePin(AX_T_PORT, A0_T_PIN, (channel & (1 << 0)));
	HAL_GPIO_WritePin(AX_T_PORT, A1_T_PIN, (channel & (1 << 1)));
	HAL_GPIO_WritePin(AX_T_PORT, A2_T_PIN, (channel & (1 << 2)));
	HAL_GPIO_WritePin(AX_T_PORT, A3_T_PIN, (channel & (1 << 3)));
}

void adc_dmx_select_none(void)
{
	adc_dmx_select(0xF);
}

void adc_stop_convertion(void)
{
	HAL_GPIO_WritePin(CS1_PORT, CS1_PIN, 1);
	HAL_GPIO_WritePin(CS2_PORT, CS1_PIN, 1);
}

void adc_start_convertion(void)
{
	// unselect all mcu
	HAL_GPIO_WritePin(CS1_PORT, CS1_PIN, 1);
	HAL_GPIO_WritePin(CS2_PORT, CS2_PIN, 1);

	// send command to external ADC
	if(adc.channel < 4)
	{
		// first adc mcu
		// gpio set chip select 1
		HAL_GPIO_WritePin(CS1_PORT, CS1_PIN, 0);
	}else{
		HAL_GPIO_WritePin(CS2_PORT, CS2_PIN, 0);
	}
}

void adc_dmx_set_next_sample(void)
{
	++adc.sample;
	if(adc.sample >= SAMPLES_IN_BUFFER)
		adc.sample = 0;
}

void adc_dmx_set_next_channel(void)
{
	++adc.channel;
	if(adc.channel >= ADC_CHANNELS_NUM)
	{
		adc.channel = 0;
		adc_dmx_set_next_sample();
	}
	adc_dmx_select(adc.channel);
}

uint8_t adc_dmx_channel(void)
{
	return adc.channel;
}

// called after read, for current channel
static void check_overflow(void) 
{
	uint32_t ovf_flags = OVL_FLAG | OVL_FLAG;
	uint32_t flags = 0;
	
	for(uint8_t i = 0; i < SAMPLES_IN_BUFFER; ++i)
	{
		flags |= adc.value[adc.channel][i] & ovf_flags;
	}
	if(flags & ovf_flags)
	{
		if(flags & OVL_FLAG)
			errors_on_error(ERROR_OVL_FLAG);
		if(flags & OVH_FLAG)
			errors_on_error(ERROR_OVH_FLAG);
		adc.ovf_flags |= (1 << adc.channel);
	}
}

static void read_value(void)
{
	HAL_StatusTypeDef st = HAL_SPI_Receive(&hspi1, &adc.value[adc.channel][adc.sample * SAMPLE_SIZE_BYTES], SAMPLE_SIZE_BYTES, 200); // clock pol high
	if(st)
		errors_on_error(ERROR_SPI);
	if(adc.result_id < SAMPLES_IN_BUFFER)
		++adc.result_id;
}

void adc_update_value(void)
{
	read_value();
	check_overflow();
}

uint8_t adc_flags(void)
{
	return adc.ovf_flags;
}

// return value multiplied by 1000
int32_t adc_value(uint8_t channel) // 0 .. 7
{
	int32_t val = 0;
	uint8_t num_samples = (adc.result_id < SAMPLES_IN_BUFFER) ? adc.result_id : SAMPLES_IN_BUFFER;
	if(!num_samples)
		return 0;
	for(uint8_t i = 0; i < num_samples * SAMPLE_SIZE_BYTES; i += SAMPLE_SIZE_BYTES)
	{
		uint32_t val_22bit = (uint32_t)((adc.value[channel][i] << 16) | (adc.value[channel][i + 1] << 8) | adc.value[channel][i + 2]);
		int32_t signed_val = (val_22bit & (1 << 21)) ? (val_22bit | (0x3FF << 22)) : val_22bit;
		val += signed_val;
	}
	val /= num_samples;
	return val;
}
