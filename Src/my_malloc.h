#ifndef __MY_MALLOC__
#define __MY_MALLOC__

typedef struct
{
	uint32_t allocated;
	uint32_t freed;
	uint32_t free_heap;
}my_alloc_t;
void* my_malloc (size_t);
void my_free (void*, size_t);
void my_alloc_init(void);
uint32_t mem_in_use(void);
void my_alloc_draw(void);
#endif
