#include "main.h"
#include "stm32f0xx.h"
#include "modbus_device.h"
#include "modbus.h"
#include "dma_usart_idle.h"
#include <stdbool.h>
#include <string.h>
#include "ext_mem.h"
#include "log.h"
#include "errors.h"
#include "soft_timer.h"
#include "version.h"
#include "adc.h"

#define MODULE_ID   21802
#define PCB_VER     1
#define PCB_MNF     1 // manufacturer
#define PCB_ASSEM   1
#define SW_VER      MODULE_VERSION_MAJOR
#define SW_SUBVER   MODULE_VERSION_MINOR
#define SW_BUILD    1

device_t device;

uint16_t mbd_module_id(void)
{
    return MODULE_ID;
}

uint32_t update_time(void)
{
    uint32_t counter = HAL_GetTick() / 1000;
    if(counter < device.counter) // если произошло переполнение
    {
        ++device.count_ovr;
    }
    device.counter = counter;
    device.operation_time = device.start_time + device.counter + device.count_ovr * (0xFFFFFFFF / 1000); // секунд
    return device.operation_time;
}

uint32_t operation_time(void)
{
    return device.operation_time;
}

void mbd_exit_init_mode(void)
{
    device.is_init = false;
    INIT_MODE_LED_PORT->ODR &= ~INIT_MODE_LED_PIN;
    mbd_rs485_init(device.baudrate);
}

static void set_spare_output(uint16_t value)
{
    if(value)
    {
        device.is_init = false;
        soft_timer_init(1, (void (*)(uint8_t))&mbd_exit_init_mode, 1, 0);
        SPARE_INIT_PORT->ODR |= SPARE_INIT_PIN;
    }else{
        SPARE_INIT_PORT->ODR &= ~SPARE_INIT_PIN;
    }
}

// oooooooooo  ooooooooooo      o      ooooooooo        oooooooooo   o      oooooooooo       o      oooo     oooo
//  888    888  888    88      888      888    88o       888    888 888      888    888     888      8888o   888
//  888oooo88   888ooo8       8  88     888    888       888oooo88 8  88     888oooo88     8  88     88 888o8 88
//  888  88o    888    oo    8oooo88    888    888       888      8oooo88    888  88o     8oooo88    88  888  88
// o888o  88o8 o888ooo8888 o88o  o888o o888ooo88        o888o   o88o  o888o o888o  88o8 o88o  o888o o88o  8  o88o

int8_t mbd_holding_reg(uint16_t reg, uint16_t* value)
{
    switch(reg)
    {
        case 0: *value = MODULE_ID;
            return 0;
        case 1: *value = PCB_VER;
            return 0;
        case 2: *value = PCB_MNF; // manufacturer
            return 0;
        case 3: *value = PCB_ASSEM;
            return 0;
        case 4: *value = SW_VER;
            return 0;
        case 5: *value = SW_SUBVER;
            return 0;
        case 6: *value = SW_BUILD;
            return 0;
        case 7:
            *value = device.operation_time >> 16;
            return 0;
        case 8:
            *value = device.operation_time & 0xFFFF;
            return 0;
        case 9:
            *value = HAL_GetTick() % 1000; // CURR_MS
            return 0;
        case 10:
            *value = device.program_crc;
            return 0;
        case 11:
            *value = device.ext_mem_crc;
            return 0;
        case 12:
            *value = device.change_time >> 16;
            return 0;
        case 13:
            *value = device.change_time & 0xFFFF;
            return 0;
        case 14:
            *value = device.total_rq >> 16;
            return 0;
        case 15:
            *value = device.total_rq & 0xFFFF;
            return 0;
        case 16:
            *value = device.handled_rq >> 16;
            return 0;
        case 17:
            *value = device.handled_rq & 0xFFFF;
            return 0;
        case 18:
            *value = device.err_crc_rq >> 16;
            return 0;
        case 19:
            *value = device.err_crc_rq & 0xFFFF;
            return 0;
        case 20:
            *value = device.err_param_rq >> 16;
            return 0;
        case 21:
            *value = device.err_param_rq & 0xFFFF;
            return 0;

        case RS485_ADDRESS_REG:
            *value = device.addr;
            return 0;
        case RS485_BAUDRATE_HREG:
            *value = device.baudrate >> 16;
            return 0;
        case RS485_BAUDRATE_LREG:
            *value = device.baudrate & 0xFFFF;
            return 0;   

        case SPARE_INIT:
            *value = (SPARE_INIT_PORT->ODR & SPARE_INIT_PIN) ? 1 : 0;
            return 0;

        case CALIB_COEFF:
            *value = device.calib_coeff;
            return 0;
        case CALIB_OFFSET:
            *value = device.calib_offset;
            return 0;
        default:
            break;
    }
    return DATA_ADDR_ERR;
}

// oooooooooo  ooooooooooo      o      ooooooooo        ooooo oooo   oooo oooooooooo ooooo  oooo ooooooooooo
//  888    888  888    88      888      888    88o       888   8888o  88   888    888 888    88  88  888  88
//  888oooo88   888ooo8       8  88     888    888       888   88 888o88   888oooo88  888    88      888
//  888  88o    888    oo    8oooo88    888    888       888   88   8888   888        888    88      888
// o888o  88o8 o888ooo8888 o88o  o888o o888ooo88        o888o o88o    88  o888o        888oo88      o888o

// здесь только значения результаты обработки входов
int8_t mbd_input_reg(uint16_t reg, uint16_t* value)
{
    switch(reg)
    {
        case RESULT_ID_0:
            *value = device.input[0].result_id;
            return 0;
        case FLAGS_0:
            *value = device.input[0].flags;
            return 0;
        case VAL_0_0:
            *value = device.input[0].val >> 16;
            return 0;
        case VAL_0_1:
            *value = device.input[0].val & 0xFFFF;
            return 0;

        case RESULT_ID_1:
            *value = device.input[1].result_id;
            return 0;
        case FLAGS_1:
            *value = device.input[1].flags;
            return 0;
        case VAL_1_0:
            *value = device.input[1].val >> 16;
            return 0;
        case VAL_1_1:
            *value = device.input[1].val & 0xFFFF;
            return 0;

        case RESULT_ID_2:
            *value = device.input[2].result_id;
            return 0;
        case FLAGS_2:
            *value = device.input[2].flags;
            return 0;
        case VAL_2_0:
            *value = device.input[2].val >> 16;
            return 0;
        case VAL_2_1:
            *value = device.input[2].val & 0xFFFF;
            return 0;

        case RESULT_ID_3:
            *value = device.input[3].result_id;
            return 0;
        case FLAGS_3:
            *value = device.input[3].flags;
            return 0;
        case VAL_3_0:
            *value = device.input[3].val >> 16;
            return 0;
        case VAL_3_1:
            *value = device.input[3].val & 0xFFFF;
            return 0;

        case RESULT_ID_4:
            *value = device.input[4].result_id;
            return 0;
        case FLAGS_4:
            *value = device.input[4].flags;
            return 0;
        case VAL_4_0:
            *value = device.input[4].val >> 16;
            return 0;
        case VAL_4_1:
            *value = device.input[4].val & 0xFFFF;
            return 0;

        case RESULT_ID_5:
            *value = device.input[5].result_id;
            return 0;
        case FLAGS_5:
            *value = device.input[5].flags;
            return 0;
        case VAL_5_0:
            *value = device.input[5].val >> 16;
            return 0;
        case VAL_5_1:
            *value = device.input[5].val & 0xFFFF;
            return 0;

        case RESULT_ID_6:
            *value = device.input[6].result_id;
            return 0;
        case FLAGS_6:
            *value = device.input[6].flags;
            return 0;
        case VAL_6_0:
            *value = device.input[6].val >> 16;
            return 0;
        case VAL_6_1:
            *value = device.input[6].val & 0xFFFF;
            return 0;

        case RESULT_ID_7:
            *value = device.input[7].result_id;
            return 0;
        case FLAGS_7:
            *value = device.input[7].flags;
            return 0;
        case VAL_7_0:
            *value = device.input[7].val >> 16;
            return 0;
        case VAL_7_1:
            *value = device.input[7].val & 0xFFFF;
            return 0;

        default:
            break;
    }
    return DATA_ADDR_ERR;
}

void init_input(uint8_t DI)
{
#ifdef _UNITTEST_
    printf("%s\n", __FUNCTION__);
#endif
    memset(&device.input[DI], 0, sizeof(input_t));
}

// oooo     oooo oooooooooo  ooooo ooooooooooo ooooooooooo      oooooooooo   o      oooooooooo       o      oooo     oooo
//  88   88  88   888    888  888  88  888  88  888    88        888    888 888      888    888     888      8888o   888
//   88 888 88    888oooo88   888      888      888ooo8          888oooo88 8  88     888oooo88     8  88     88 888o8 88
//    888 888     888  88o    888      888      888    oo        888      8oooo88    888  88o     8oooo88    88  888  88
//     8   8     o888o  88o8 o888o    o888o    o888ooo8888      o888o   o88o  o888o o888o  88o8 o88o  o888o o88o  8  o88o

int8_t mbd_write_reg(uint16_t reg, uint16_t value)
{
    bool update_ext_mem = false;
    switch(reg)
    {
        case SPARE_INIT:
            set_spare_output(value);
            break;
        case RS485_ADDRESS_REG:
            if(device.is_init)
            {
                device.addr = value;
                update_ext_mem = true;
            }
            break;
        case RS485_BAUDRATE_HREG:
            if(device.is_init)
            {
                device.baudrate &= ~(0xFFFF << 16);
                device.baudrate |= value << 16;
            }
            // no update here
            break;
        case RS485_BAUDRATE_LREG:
            if(device.is_init)
            {
                device.baudrate &= ~0xFFFF;
                device.baudrate |= value;
                update_ext_mem = true;
            }
            break;
        case CALIB_COEFF:
            device.calib_coeff = value;
            update_ext_mem = true;
            break;
        case CALIB_OFFSET:
            device.calib_offset = value;
            update_ext_mem = true;
            break;
        default:
            return DATA_ADDR_ERR;
    }
    if(update_ext_mem)
    {
        int8_t ret = ext_mem_store();
        if(ret)
        {
            ERROR(EXT_MEM_WRITE_ERROR);
            return FATAL_ERR;
        }
    }
    return 0;
}

void mbd_inc_crc_err_rq(void)
{
    ++device.err_crc_rq;
}

void mbd_inc_handled_rq(void)
{
    ++device.handled_rq;
}

void mbd_inc_total_rq(void)
{
    ++device.total_rq;
}

void mdb_inc_send_err(void)
{
    ++device.err_send;
}

void mbd_process(uint8_t input) // 0..7
{
    if(!device.calib_coeff)
    {
        device.calib_coeff = DEFAULT_CALIB_COEFF;
    }
    if(!device.calib_offset)
    {
        device.calib_offset = DEFAULT_CALIB_OFFSET;
    }
    int32_t val = adc_value(input);
    if(val)
    {
       device.input[input].val = (int32_t)((float)val * ((float)device.calib_coeff / 1000) / (2.048 * 256)) + device.calib_offset;
       ++device.input[input].result_id;
       device.input[input].flags = (adc_flags() & (1 << input)) ? 1 : 0;
    }
}

extern UART_HandleTypeDef huart4;

int8_t mbd_rs485_init(uint32_t baudrate)
{
    if(!baudrate)
        return -1;
    HAL_NVIC_DisableIRQ(USART3_8_IRQn);
    HAL_UART_DeInit(&huart4);
    huart4.Init.BaudRate = baudrate;
    huart4.Init.WordLength = UART_WORDLENGTH_8B;
    huart4.Init.StopBits = UART_STOPBITS_1;
    huart4.Init.Parity = UART_PARITY_NONE;
    huart4.Init.Mode = UART_MODE_TX_RX;
    huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart4.Init.OverSampling = UART_OVERSAMPLING_16;
    huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
    huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    HAL_RS485Ex_Init(&huart4, UART_DE_POLARITY_HIGH, 31, 31);
    if (HAL_UART_Init(&huart4) != HAL_OK)
    {
        return -1;
    }
    dma_usart_idle_reinit();
    HAL_NVIC_EnableIRQ(USART3_8_IRQn);
    return 0;
}

void mbd_init(void)
{
    device.is_init = false;
    device.baudrate = huart4.Init.BaudRate;
    device.calib_coeff = DEFAULT_CALIB_COEFF;
    device.calib_offset = DEFAULT_CALIB_OFFSET;
    if(ext_mem_load())
    {
        ERROR(CONFIG_LOAD_ERROR);
    }
    mbd_rs485_init(device.baudrate);
}

#define GET_TIM_CCR(__HANDLE__, __CHANNEL__) \
    (*(__IO uint16_t *)(&((__HANDLE__)->Instance->CCR1) + (__CHANNEL__ - 1)))

void mbd_common_params(common_params_t* cp)
{
    cp->addr            = device.addr;
    cp->module_id       = MODULE_ID;
    cp->operation_time  = device.operation_time;
    cp->program_crc     = device.program_crc;
    cp->total_rq        = device.total_rq;      // счетчик запросов
    cp->handled_rq      = device.handled_rq;    // счетчик обработанных запросов
    cp->err_crc_rq      = device.err_crc_rq;    // счетчик запросов с ошибкой CRC
    cp->err_param_rq    = device.err_param_rq;  // счетчик запросов с некорректными параметрами
    cp->err_send        = device.err_send;      // счетчик ошибок при отправке
    cp->change_time     = device.operation_time;
    cp->baudrate        = device.baudrate;
    cp->calib_coeff     = device.calib_coeff;
    cp->calib_offset    = device.calib_offset;
}

void mbd_log_status(void)
{
    log_out("start_time:\t %lu \n",     device.start_time);
    log_out("program_crc:\t %u \n",     device.program_crc);
    log_out("total_rq:\t %lu \n",       device.total_rq);
    log_out("handled_rq:\t %lu \n",     device.handled_rq);
    log_out("err_crc_rq:\t %lu \n",     device.err_crc_rq);
    log_out("err_param_rq:\t %lu \n",   device.err_param_rq);
    log_out("err_send:\t %lu \n",       device.err_send);
    log_out("change_time:\t %lu\n",     device.change_time);
    log_out("calib_coeff:\t %lu\n",     device.calib_coeff);
}

void mbd_log_config(void)
{
    log_out("---------------- AI config ----------------\n");
    log_out("AI baudrate: %lu\n", huart4.Init.BaudRate);
}

void mbd_set_common_params(common_params_t* cp)
{
    if(cp->addr)
        device.addr     = cp->addr;
    device.start_time   = cp->operation_time;
    device.program_crc  = cp->program_crc;
    device.total_rq     = cp->total_rq;     // счетчик запросов
    device.handled_rq   = cp->handled_rq;   // счетчик обработанных запросов
    device.err_crc_rq   = cp->err_crc_rq;   // счетчик запросов с ошибкой CRC
    device.err_param_rq = cp->err_param_rq; // счетчик запросов с некорректными параметрами
    device.err_send     = cp->err_send;     // счетчик ошибок при отправке
    device.change_time  = cp->change_time;
    device.baudrate     = cp->baudrate;
    device.calib_coeff  = cp->calib_coeff;
    device.calib_offset = cp->calib_offset;
}

void mbd_set_baudrate(uint32_t baudrate)
{
    device.baudrate = baudrate;
    mbd_rs485_init(device.baudrate);
}

uint8_t mbd_addr(void)
{
    if(!device.addr)
        return 1;
    return device.addr;
}

#define IS_EOF_INIT() (SPARE_INIT_PORT->ODR & SPARE_INIT_PIN)

bool mbd_can_init(void)
{
    if(device.is_init)
        return false;
    if(IS_EOF_INIT())
        return false;
    return true;
}

bool mbd_is_init(void)
{
    return device.is_init;
}

void mbd_int_init(void)
{
    device.is_init = true;
    INIT_MODE_LED_PORT->ODR |= INIT_MODE_LED_PIN;
    mbd_rs485_init(INT_INIT_BAUDRATE); // 4800
}

void mbd_ext_init(void)
{
    device.is_init = true;
    INIT_MODE_LED_PORT->ODR |= INIT_MODE_LED_PIN;
    mbd_rs485_init(EXT_INIT_BAUDRATE); // 38400
}
