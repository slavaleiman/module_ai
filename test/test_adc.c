#include <stdint.h>
#include "unity.h"

typedef struct 
{
	uint8_t err_num;
	const char* const err_msg;
}error_t;

error_t errors[] = 
{
    {ERROR_OVL_FLAG, 			"АЦП превышение нижнего порога"},
    {ERROR_OVH_FLAG, 			"АЦП превышение верхнего порога"},
    {CONFIG_LOAD_ERROR, 		"ошибка чтения конфигурации"},
    {EXT_MEM_WRITE_ERROR, 		"ошибка записи во внешнюю память"},
    {RS485_OVERRUN_ERROR, 		"RS485 оверран"},
    {MODBUS_HOLDING_REG_LOCKED, "ошибка доступа к регистру хранения"},
    0
};

void setUp(void)
{
}

void tearDown(void)
{
}

uint8_t neg_values[] = 
{
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff, 
	0x3f, 0xff, 0xff
};

uint8_t value[] = 
{
	0x5, 0xfa, 0xc7, 
	0x5, 0xf8, 0x6f, 
	0x5, 0xfe, 0x6c, 
	0x5, 0xfc, 0xff, 
	0x5, 0xfd, 0xca, 
	0x5, 0xfd, 0xd4, 
	0x5, 0xfb, 0x7f, 
	0x5, 0xf9, 0x76, 
	0x5, 0xf8, 0x1d, 
	0x5, 0xfc, 0x47, 
	0x5, 0xf9, 0x69, 
	0x5, 0xf8, 0xc2, 
	0x5, 0xfb, 0x5d, 
	0x5, 0xfc, 0x37, 
	0x5, 0xfd, 0x5d, 
	0x5, 0xf8, 0xef
};

int32_t adc_value(void) // 0 .. 7
{
	int32_t val = 0;
	uint8_t num_samples = 16; // (mod.result_id < SAMPLES_IN_BUFFER) ? mod.result_id : SAMPLES_IN_BUFFER;
	if(!num_samples)
		return 0;

	for(uint8_t i = 0; i < num_samples * 3; i += 3)
	{
		uint32_t val_22bit = (uint32_t)((value[i] << 16) | (value[i + 1] << 8) | value[i + 2]);
		if(val_22bit)
		{
			int32_t signed_val = (val_22bit & (1 << 21)) ? (val_22bit | (0x3FF << 22)) : val_22bit;
			printf("signed_val = %d\n", signed_val);
			printf("signed_val = %x\n", signed_val);
			val += signed_val;
		}
	}
	val /= num_samples;
	return val;
}

void test_adc_value_average(void)
{
	int32_t val = adc_value();
	printf("val = %d\n", val);
	TEST_ASSERT_GREATER_THAN(0, val);
}

void test_adc_errors(void)
{
	printf("%s\n", );
}

int main(void)
{
    UNITY_BEGIN();
	RUN_TEST(test_adc_value_average);
	RUN_TEST(test_adc_errors);

    return UNITY_END();
}
