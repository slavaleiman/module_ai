/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
/* USER CODE BEGIN Private defines */
#define DEBUG 0
#define MODULE_NAME "AI.0.001"

#define SPARE_INIT_PIN      (1 << 13)
#define SPARE_INIT_PORT     GPIOB

#define WATCH_MODBUS_TASK   (1 << 0)
#define WATCH_INPUT_TASK    (1 << 1)
#define WATCH_LOG_TASK      (1 << 2)

#define WATCHDOG_OUTPUT_PIN (1 << 12)
#define WATCHDOG_OUTPUT_PORT GPIOA
#define WATCH_FLAGS_ALL     (WATCH_LOG_TASK | WATCH_INPUT_TASK | WATCH_MODBUS_TASK)

#define EXT_INIT_PORT       GPIOB
#define EXT_INIT_PIN        (1 << 15)

#define INT_INIT_PORT       GPIOB
#define INT_INIT_PIN        (1 << 14)

#define INIT_MODE_LED_PORT  GPIOB
#define INIT_MODE_LED_PIN   (1 << 8)

#define FAULT_LED_PORT  GPIOD
#define FAULT_LED_PIN   (1 << 2)
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
