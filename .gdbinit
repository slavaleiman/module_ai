tar ext :4242
set print pretty on
set pagination off

define killtims
    set TIM6->CR1 &= ~1
    set TIM1->CR1 &= ~1
    set TIM2->CR1 &= ~1
    set TIM3->CR1 &= ~1
    set TIM7->CR1 &= ~1    
end

define killall
    set TIM6->CR1 &= ~1
    set TIM1->CR1 &= ~1
    set TIM2->CR1 &= ~1
    set TIM3->CR1 &= ~1
    set TIM7->CR1 &= ~1
    set USART4->CR1 &= ~1
end

define resumeall
    set TIM6->CR1 |= 1
    set TIM1->CR1 |= 1
    set TIM2->CR1 |= 1
    set TIM3->CR1 |= 1
    set TIM7->CR1 |= 1
    set USART4->CR1 |= 1
end

define t1
    p/t *TIM1
end

define t2
    p/t *TIM2
end

define u4
    p/t *USART4
end

define h4
    p/t* hdma_usart4_rx->Instance
end 

define in
    p/t device.input[$arg0]
end

define inreg
    p/t device.input_reg
end

define setall
    set device.input[0].mode = $arg0
    set device.input[1].mode = $arg0
    set device.input[2].mode = $arg0
    set device.input[3].mode = $arg0
    set device.input[4].mode = $arg0
    set device.input[5].mode = $arg0
    set device.input[6].mode = $arg0
    set device.input[7].mode = $arg0
end

define water_marks
    p modbusHighWaterMark
    p inputHighWaterMark
    p tickHighWaterMark
end

define status
    p device
    printf "current buadrate %d\n", huart4->Init->BaudRate
end

b HardFault_Handler
b mbd_process
commands
p device.input[3].val
c
end
b dma_usart_idle_init
#b USART3_8_IRQHandler
b modbus_on_rtu
