#!python3

ai_holding_regs = {
	#                  	 addr  len
	"MODULE_ID":         [0,	1, 	0,	0],
	"TIME":              [7,	2, 	0,	0],
	"ms":                [9,	1, 	0,	0],
	"total":             [14, 	1, 	0,	0],
	"handled":           [16, 	1, 	0,	0],
	"crc":               [18, 	2, 	0,	0],
	"err_param":         [20, 	2, 	0,	0],

	"RS485_ADDRESS":     [22, 	1, 	0,	0],
	"RS485_BAUDRATE":    [23, 	2, 	0,	0],

	"SPARE-INIT":        [57, 	1, 	0,	0],
    "CALIB_COEFF":       [58, 1, 0, 0],
    "CALIB_OFFSET":      [59, 1, 0, 0]
}

ai_input_regs = {
    "RESULT_ID_0": 	[25, 1, 0, 0],
    "FLAGS_0":		[26, 1, 0, 0],
    "VAL_0":  		[27, 2, 0, 0],

    "RESULT_ID_1": 	[29, 1, 0, 0],
    "FLAGS_1": 		[30, 1, 0, 0],
    "VAL_1": 		[31, 2, 0, 0],

    "RESULT_ID_2": 	[33, 1, 0, 0],
    "FLAGS_2": 		[34, 1, 0, 0],
    "VAL_2": 		[35, 2, 0, 0],

    "RESULT_ID_3": 	[37, 1, 0, 0],
    "FLAGS_3": 		[38, 1, 0, 0],
    "VAL_3": 		[39, 2, 0, 0],

    "RESULT_ID_4": 	[41, 1, 0, 0],
    "FLAGS_4": 		[42, 1, 0, 0],
    "VAL_4": 		[43, 2, 0, 0],

    "RESULT_ID_5": 	[45, 1, 0, 0],
    "FLAGS_5": 		[46, 1, 0, 0],
    "VAL_5": 		[47, 2, 0, 0],

    "RESULT_ID_6": 	[49, 1, 0, 0],
    "FLAGS_6": 		[50, 1, 0, 0],
    "VAL_6": 		[51, 2, 0, 0],

    "RESULT_ID_7": 	[53, 1, 0, 0],
    "FLAGS_7": 		[54, 1, 0, 0],
    "VAL_7": 		[55, 2, 0, 0],
}
