#!/usr/bin/python3
from modbus import *
import sys

if len(sys.argv) > 3:
	set_port(sys.argv[1])
	device_addr = sys.argv[2]
	regnum = sys.argv[3]
else:
	print("USAGE: %s PORT DEVICE_ADDR REGISTER")

while True:
	read_input_reg(device_addr, int(regnum), 2)
	sleep(0.2)
